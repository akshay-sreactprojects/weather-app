import { City } from "../models/City";

export const cities: City[] = [
  { name: "Bamberg", id: 2952984 },
  { name: "Barcelona", id: 1726701 },
  { name: "Boston", id: 4930956 },
  { name: "Colmar", id: 3024297 },
  { name: "Freiburg", id: 2925177 },
  { name: "Glasgow", id: 2648579 },
  { name: "Gurugram", id: 1270642 },
  { name: "London", id: 2643743 },
  { name: "Tokyo", id: 1850147 },
  { name: "New York", id: 5128638 },
  { name: "San Francisco", id: 5391959 },
  { name: "Paris", id: 2968815},
  { name: "Madrid", id: 3117735},
  { name: "Chicago", id: 4887398},
  { name: "Moscow", id: 524894}
];
