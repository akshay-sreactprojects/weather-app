import React from "react";
import styles from "./App.module.css";
import PersistentDrawerLeft from "./components/PersistentDrawer";

import { City } from "./models/City";
import { cities } from "./utils/Constants";

import WeatherForecast from "./components/WeatherForecast";

const App: React.FunctionComponent = () => {
  const [city, setCity] = React.useState<City>(cities[0]);

  const handleCityChange = (city: City) => {
    setCity(city)
  }

  return (
    <div className={styles.App}>
      <PersistentDrawerLeft drawerText={'Displaying Weather Information for ' + city.name} />
      <WeatherForecast city={city} setCity={handleCityChange} />
    </div>
  );
};

export default App;