import React from "react";

import { City } from "../models/City";
import { cities } from "../utils/Constants";

import CitySelector from "./CitySelector";

type WeatherForecastProps = {
  city: City;
  setCity: (city: City) => void
}

const WeatherForecast: React.FunctionComponent<WeatherForecastProps> = (props: WeatherForecastProps) => {

  return (
    <div>
      {/* 
        Dropdown component consisting of all the cities
      */}
      <CitySelector
        cities={cities}
        initialCityId={props.city.id.toString()}
        onCityChange={props.setCity}
      />
    </div>
  );
};

export default WeatherForecast;
